@
@The ADD/ADC 64bits
@
.global _start
_start:
	mov R2,#0x00000003
	mov R3,#0xFFFFFFFF
	mov R4,#0x00000005
	mov R5,#0x00000001
	
	adds R1, R3, R5
	ADC  R0, R2, R4

	mov R7,#1
	svc 0		@call Linux to terminate
