@Assembler program to print "Hello World"
@to stdout
@
.global _start	@provide program starting
@set up the parameters to print hello world

_start: 
	mov R0,#1
	ldr R1,=helloworld
	mov R2,#13
	mov R7,#4
	svc 0			@call linux to print

	mov R0,#0
	mov R7,#1
	svc 0			@call linux to terminate

	.data

	helloworld:	.ascii "Hello World\n"

