@
@ Examples of the mov instruction
@
.global _start
@ Load R2 using mov
_start:
	MOV 	R2,#0x6E3A
	MOVT	R2,#0x4F5D
	MOV		R1,R2
	MOV		R1,R2,LSL #1
	MOV		R1,R2,LSR #1
	MOV		R1,R2,ASR #1
	MOV		R1,R2,ROR #1
	MOV		R1,R2,RRX
@Repeat the above shifts using the Assembler mnemonics
	LSL 	R1,R2,#1
	LSR		R1,R2,#1
	ASR		R1,R2,#1
	ROR		R1,R2,#1
	RRX		R1,R2
@Example that works with 8 bit
	MOV		R1,#0xAB000000
@Example of mvn
	MVN		R1,#45
	MOV		R1,#0xFFFFFFFE
@Setup the parameters to exit the program and then call Linux to do it
	MOV		R0,#0
	mov		R7,#1
	svc		0
